object Form1: TForm1
  Left = 131
  Top = 78
  Width = 823
  Height = 631
  Caption = 'ABS Simulator'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 665
    Height = 401
  end
  object SliprateLabel: TLabel
    Left = 128
    Top = 536
    Width = 456
    Height = 13
    Caption = 
      '0%                                     Slip Rate (Taux de glisse' +
      'ment)                      100%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BrakeButton: TButton
    Left = 16
    Top = 520
    Width = 81
    Height = 17
    Caption = 'Freiner'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = BrakeButtonClick
  end
  object Panel1: TPanel
    Left = 680
    Top = 8
    Width = 129
    Height = 377
    Color = clSkyBlue
    TabOrder = 1
    object WeatherLabel: TLabel
      Left = 9
      Top = 8
      Width = 98
      Height = 13
      Caption = 'Conditions m'#233't'#233'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Image2: TImage
      Left = 8
      Top = 48
      Width = 49
      Height = 49
    end
    object Image3: TImage
      Left = 8
      Top = 104
      Width = 89
      Height = 65
    end
    object Image4: TImage
      Left = 56
      Top = 304
      Width = 65
      Height = 41
      Picture.Data = {
        07544269746D61709A1F0000424D9A1F00000000000036000000280000004100
        0000290000000100180000000000641F00000000000000000000000000000000
        0000800000800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000008080008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        80000080000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF000080000080000000808000808000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF000080000080000000808000808000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF000080000080000000808000808000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF000080000080000000808000808000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000800000800000008080
        00808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000008000
        0000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000080
        000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF000080000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF000080000080000000808000808000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF000080000080000000808000808000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF000080000080000000808000808000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF00006800F00000FFF00068FF0000FF0000FF0000FF00
        00FF00004D00F86800F0FF00000000FF0000FF0000FF0000FF4D00F8A700D0F0
        0068FF0000FF0000FF0000D000A77C00E90000FF0000FFA700D0FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF000080000080000000808000808000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF00009A00D90000FFD000A7FF0000FF0000FF
        0000FF0000D000A70000FFB200C7FF00000000FF0000FF0000FF0000FF0000FF
        0000FF4D00F8F00068FF0000FF00000000FF0000FF0000FF0000FF0000FF4D00
        F8FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000800000800000008080
        00808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000C700B20000FFA700D0FF0000
        FF0000FF0000FF00009A00D90000FFE9007CFF00000000FF0000FFFF0000FF00
        00F000689A00D90000FF9A00D9FF0000FF00009A00D9E9007CFF0000F000687C
        00E90000FFA700D0FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000008000
        0000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000E1008C0000FF7C00
        E9FF0000FF0000FF0000F000680000FF6800F0FF0000FF00000000FF0000FFFF
        0000FF0000FF0000FF00000000FF0000FFFF0000FF0000FF0000FF0000FF0000
        FF0000F000680000FF4D00F8FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000080
        000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000
        00FF0000FF0000FF0000FF0000FF0000FF0000FFB200C7FF0000FF00000000FF
        0000FFFF0000FF0000FF0000FF00000000FF0000FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF00000000FF0000FFFF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF000080000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF00008C00E10000FF0000FF0000FF0000FF0000FF0000FFD9009AFF0000FF00
        000000FF0000FFFF0000FF0000F00068A700D00000FF8C00E1FF0000FF0000FF
        0000FF0000FF0000FF0000C700B20000FF6800F0FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF000080000080000000808000808000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000B200C70000FFFF0000FF0000F000680000FF4D00F8FF0000FF
        0000FF00000000FF0000FF0000FF0000FF0000FF0000FF6800F0E9007CFF0000
        FF0000FF0000FF0000E1008C8C00E10000FF0000FFD9009AFF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF000080000080000000808000808000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000D9009A0000FFD000A7FF0000D000A70000FFA700D0
        FF0000FF0000FF00000000FF0000FF0000FF0000FF0000FFC700B2FF0000FF00
        00FF0000FF0000FF00008C00E10000FF0000FF6800F0D9009AFF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF000080000080000000808000808000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF00000000FF9A00D9FF00009A00D90000
        FFD9009AFF0000FF0000FF00000000FF0000FFFF0000FF0000A700D04D00F8E9
        007CFF0000FF0000FF0000A700D00000FF6800F0D000A7FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF000080000080000000808000808000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF00006800F00000FFF0006800
        00FF0000FFFF0000FF0000FF0000FF00000000FF0000FFFF0000FF0000FF0000
        0000FF7C00E9FF0000FF0000FF00004D00F80000FFE9007CFF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000800000800000008080
        00808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000A700D00000FF
        8C00E10000FF9A00D9FF0000FF0000FF0000FF00000000FF0000FFFF0000FF00
        00FF00000000FF0000FFFF0000FF0000FF00000000FF0000FFFF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000008000
        0000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000C700
        B20000FF0000FF0000FFD000A7FF0000FF0000FF0000FF00000000FF0000FFFF
        0000FF0000BD00BD0000FF4D00F8FF0000FF0000FF00007C00E90000FFA700D0
        FF0000F000689A00D9FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000080
        000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000E9007C0000FF0000FF0000FFFF0000FF0000FF0000FF0000FF00000000FF
        0000FF0000FF0000FF0000FF0000FFB200C7FF0000FF0000FF0000D9009A0000
        FF0000FF0000FF0000FF0000FFFF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF000080000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF00000000FF0000FF8C00E1FF0000FF0000FF0000FF0000FF00
        000000FF0000FF0000FF0000FF4D00F8B200C7FF0000FF0000FF0000FF0000FF
        0000D9009A6800F00000FF6800F0BD00BDFF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF000080000080000000808000808000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF000080000080000000808000808000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF000080000080000000808000808000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF000080000080000000808000808000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000800000800000008080
        00808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000008000
        0000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000080
        000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF000080000080000000808000808000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF000080000080000000808000808000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF000080000080000000808000808000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF000080000080000000808000808000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF000080000080000000808000808000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000800000800000008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008000008000
        0000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        800080000000}
    end
    object GLSceneViewer1: TGLSceneViewer
      Left = 8
      Top = 176
      Width = 113
      Height = 121
      Camera = GLCamera1
      FieldOfView = 96.985183715820310000
    end
    object WeatherComboBox: TComboBox
      Left = 8
      Top = 24
      Width = 113
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      Text = 'Ensoleill'#233'e'
      OnChange = WeatherComboBoxChange
    end
    object ABSCheckBox: TCheckBox
      Left = 8
      Top = 328
      Width = 49
      Height = 17
      Caption = 'ABS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object AutoBrakeCheckBox: TCheckBox
      Left = 8
      Top = 352
      Width = 97
      Height = 17
      Caption = 'FreinageAuto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = AutoBrakeCheckBoxClick
    end
  end
  object SliprateBar: TProgressBar
    Left = 128
    Top = 512
    Width = 457
    Height = 17
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 680
    Top = 400
    Width = 129
    Height = 121
    Caption = 'Parametres de test'
    TabOrder = 3
    object KmhLabel: TLabel
      Left = 57
      Top = 96
      Width = 24
      Height = 13
      Caption = 'kmh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object InputKmhLabel: TLabel
      Left = 8
      Top = 72
      Width = 107
      Height = 13
      Caption = 'Vitesse a atteindre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GoTest: TButton
      Left = 6
      Top = 24
      Width = 75
      Height = 17
      Caption = 'Tester'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = GoTestClick
    end
    object Button3: TButton
      Left = 8
      Top = 48
      Width = 73
      Height = 17
      Caption = 'R'#233'init.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button3Click
    end
    object InputKmhEdit: TEdit
      Left = 8
      Top = 92
      Width = 41
      Height = 21
      TabOrder = 2
      Text = '60'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 558
    Width = 815
    Height = 19
    Panels = <>
  end
  object GroupBox2: TGroupBox
    Left = 128
    Top = 424
    Width = 529
    Height = 81
    Caption = 'Parametres'
    TabOrder = 5
    object kg: TLabel
      Left = 64
      Top = 40
      Width = 15
      Height = 13
      Caption = 'kg'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object InputCarWeight: TLabel
      Left = 8
      Top = 16
      Width = 89
      Height = 13
      Caption = 'Masse vehicule'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 136
      Top = 16
      Width = 104
      Height = 13
      Caption = 'Boite Automatique'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object WeatherCdtLabel: TLabel
      Left = 264
      Top = 16
      Width = 74
      Height = 13
      Caption = 'Condition sol'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object WeatherTypeLabel: TLabel
      Left = 264
      Top = 32
      Width = 85
      Height = 13
      Caption = 'Bonne adh'#233'rence'
    end
    object DiscStatusLabel: TLabel
      Left = 400
      Top = 16
      Width = 112
      Height = 13
      Caption = 'Etat disque de frein'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DiscStatus: TLabel
      Left = 400
      Top = 32
      Width = 51
      Height = 13
      Caption = 'DiscStatus'
    end
    object CarWeightEdit: TEdit
      Left = 8
      Top = 32
      Width = 49
      Height = 21
      TabOrder = 0
      Text = '2100'
    end
    object GearAutoCheckBox: TCheckBox
      Left = 136
      Top = 32
      Width = 97
      Height = 17
      Caption = 'Activ'#233
      TabOrder = 1
    end
    object AnalysisButton: TButton
      Left = 416
      Top = 56
      Width = 97
      Height = 17
      Caption = 'Analyser'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = AnalysisButtonClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 424
    Width = 105
    Height = 89
    Caption = 'Boite de rapport'
    TabOrder = 6
    object GearLabel: TLabel
      Left = 8
      Top = 20
      Width = 57
      Height = 13
      Caption = 'Vitesse: 1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GearUp: TButton
      Left = 8
      Top = 40
      Width = 57
      Height = 17
      Caption = 'Monter'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = GearUpClick
    end
    object GearDown: TButton
      Left = 8
      Top = 64
      Width = 81
      Height = 17
      Caption = 'Descendre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = GearDownClick
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 1
    OnTimer = Timer1Timer
    Left = 16
    Top = 16
  end
  object GLScene1: TGLScene
    Left = 16
    Top = 48
    object GLLightSource1: TGLLightSource
      ConstAttenuation = 1.000000000000000000
      Position.Coordinates = {0000803F0000803F0000803F0000803F}
      SpotCutOff = 180.000000000000000000
    end
    object DiscSet: TGLDummyCube
      Direction.Coordinates = {2EBD3BB30000803F2EBD3BB300000000}
      RollAngle = 90.000000000000000000
      Scale.Coordinates = {0AD7233C0AD7233C0AD7233C00000000}
      TurnAngle = 90.000000000000000000
      Up.Coordinates = {000080BF2EBD3BB30000000000000000}
      CubeSize = 1.000000000000000000
      object DiscSupport: TGLFreeForm
      end
      object DiscPlaquette1: TGLFreeForm
      end
      object DiscPlaquette2: TGLFreeForm
      end
      object DiscEtrier: TGLFreeForm
        Material.BlendingMode = bmTransparency
        Material.FaceCulling = fcCull
      end
      object DiscBrake: TGLFreeForm
      end
      object Target1: TGLDummyCube
        CubeSize = 1.000000000000000000
      end
    end
    object GLCamera1: TGLCamera
      DepthOfView = 100.000000000000000000
      FocalLength = 50.000000000000000000
      NearPlaneBias = 0.009999999776482582
      TargetObject = Target1
      Position.Coordinates = {3333333F0000803E3333333F0000803F}
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer2Timer
    Left = 776
    Top = 424
  end
  object MainMenu1: TMainMenu
    Left = 56
    Top = 16
    object Fichier1: TMenuItem
      Caption = 'Fichier'
      object Quitter1: TMenuItem
        Caption = 'Quitter'
        OnClick = Quitter1Click
      end
    end
    object Parametres1: TMenuItem
      Caption = 'Parametres'
      object Choisirunvhicule1: TMenuItem
        Caption = 'Choisir un v'#233'hicule'
        object BMW325i1: TMenuItem
          Caption = 'BMW 325i'
          OnClick = BMW325i1Click
        end
        object Porsche9111: TMenuItem
          Caption = 'Porsche 911'
          OnClick = Porsche9111Click
        end
      end
    end
    object Aide1: TMenuItem
      Caption = 'Aide'
      object Guide1: TMenuItem
        Caption = 'Guide'
      end
      object APropos1: TMenuItem
        Caption = 'A Propos'
      end
    end
  end
  object XPManifest1: TXPManifest
    Left = 56
    Top = 48
  end
end
