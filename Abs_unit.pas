unit Abs_unit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, GLMisc, GLScene, ComCtrls, GLWin32Viewer,
  GLObjects, GLVectorFileObjects, ImgList, XPMan, Menus;

Type
  //****Concere le v�hicule****
    {Array of Points: type (x,y:integer)}
  Points=Array[1..45] of Tpoint;

    {For the gearbox}
  Gear=record
    speed,ratio:real;
  end;

  Tab=Record
    x,fx:real;
  end;

  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    BrakeButton: TButton;
    GLScene1: TGLScene;
    GLLightSource1: TGLLightSource;
    DiscSet: TGLDummyCube;
    DiscSupport: TGLFreeForm;
    DiscPlaquette1: TGLFreeForm;
    DiscPlaquette2: TGLFreeForm;
    DiscEtrier: TGLFreeForm;
    DiscBrake: TGLFreeForm;
    Panel1: TPanel;
    GLSceneViewer1: TGLSceneViewer;
    WeatherLabel: TLabel;
    Image2: TImage;
    Image3: TImage;
    WeatherComboBox: TComboBox;
    ABSCheckBox: TCheckBox;
    SliprateBar: TProgressBar;
    SliprateLabel: TLabel;
    Target1: TGLDummyCube;
    GLCamera1: TGLCamera;
    AutoBrakeCheckBox: TCheckBox;
    GroupBox1: TGroupBox;
    GoTest: TButton;
    Button3: TButton;
    InputKmhEdit: TEdit;
    KmhLabel: TLabel;
    Timer2: TTimer;
    InputKmhLabel: TLabel;
    StatusBar1: TStatusBar;
    GroupBox2: TGroupBox;
    CarWeightEdit: TEdit;
    kg: TLabel;
    InputCarWeight: TLabel;
    Label1: TLabel;
    GearAutoCheckBox: TCheckBox;
    WeatherCdtLabel: TLabel;
    WeatherTypeLabel: TLabel;
    DiscStatusLabel: TLabel;
    DiscStatus: TLabel;
    AnalysisButton: TButton;
    GroupBox3: TGroupBox;
    GearUp: TButton;
    GearDown: TButton;
    GearLabel: TLabel;
    MainMenu1: TMainMenu;
    XPManifest1: TXPManifest;
    Fichier1: TMenuItem;
    Parametres1: TMenuItem;
    Aide1: TMenuItem;
    Guide1: TMenuItem;
    APropos1: TMenuItem;
    Choisirunvhicule1: TMenuItem;
    Quitter1: TMenuItem;
    BMW325i1: TMenuItem;
    Porsche9111: TMenuItem;
    Image4: TImage;
{********************************}
{****For the Gearbox and misc****}
{********************************}

{***Procedures et fonctions pour les calculs et l'interface***}

      //A functions including the GearBox System
      {Equations horaires pour la phase d'accelleration
      Incluant le systeme de boite de vitesse}
    Function f(x:real):real;
        {fonction deriv�e de f}
    Function fdx(x:real):real;
      //A function for the phase of braking
      {Fonction pour le freinage}
    Function g(x,v0,a:real):real;

      //Loading the parts of the Disc Brake (3ds files)
      {Permet de charger l'image 3D du disque de frein
      //Fichier 3ds}
    Procedure LoadDisc;

      //A procedure to set the physics conditions, especially the slipping rate
      {Pour initialiser le taux de glissement en fonction du m�t�o}
    Procedure SetWeather(vspeed:integer;var sliprate,cur:real;wcdt:string);

      //A procedure for initializing the bitmaps
      {Pour charger les fichiers images(bitmap)
      des conditions meteo}
    Procedure LoadBitmaps;

      //A procedure to set the position of the brake pedal
      {Procedure permetant de positionner le pedal de frein
      sur l'image}
    Procedure SetPedalPos(pedalpos:integer);

      //A procedure for animating the 3d disc brake
      {Procedure permettant de faire tourner le disque
      en fonction de la vitesse du v�hicule et du taux glissement}
    Procedure AnimDisc(angspeed:real;braking:boolean);

      //A procedure to set the position of the 'etriers'
      {Permet de positionner les etriers de frein du disque)}
    Procedure SetBrakeDisc(force:Real);

      //Color of the weather
      {Fonction renvoiant une couleur � partir des cdt m�t�o}
    Function WeatherColor(weather:string):TColor;

{***********************}
{****Main Procedures****}
{***********************}

{***Procedures et fonctions pour le dessin du v�hicule***}
      //To clear the canvas
      {Pour effacer le canevas}
    Procedure Cls;

      //A circle for the canvas
      {Pour dessiner un cercle}
    Procedure Circle(Cen:TPoint;radius:integer);

      //Used to rescale
      {Fonction permettant d'agrandir/r�duire une valeur}
    Function Scl(Length:real):integer;

      //Translation based on an angle,radius and distance
      {Translation compos� de rotation et de translation}
    Procedure Translate2(var Tx,Ty:integer;r,d:integer;theta:real);

      //An User Coordinate Systems
      {Fonction: Syst�me de Coordonn�es Utilisateur SCU}
      {Note: permet de changer de rep�re}
        {These ucs is used for the vehicle}
        {Utilis� pour le dessin du v�hicule}

      {1) The first ucs is used for the main car}
      {Le premier est utilis� pour la carrosserie et ses elements}
    Function Ucs(x,y:integer):TPoint;

      {2) This second is a fixed Ucs used for the wheels}
      {Le second utilis� pour lea roues et disques}
    Function UcsF(x,y:integer):TPoint;

      {3) this last is used for the rest}
      {Utilis� pour le reste: Tachymetre, decors environementales...}
    Function UcsG(x,y:integer):TPoint;

{*************************************************}
{****An elements for the interface environment****}
{*************************************************}
      //Draw a speedmeter
      {Dessine une barre horizontal pour mieux voir le mouvement du v�hicule}
    Procedure Speedmeter(var x_pos:integer;mycolor:TColor);

      //A mobile background
      {Dessine l'arri�re-plan: un pont}
    Procedure Bridge(var x_pos:integer);

      //Wow! A tachyometer
      {Dessine le tachymetre pour la vitesse}
      {Et un tachometre pour le r�gime du moteur}
    Procedure tachometer(x,y:integer;speed,tmax:real);
    Procedure tachymeter(x,y:integer;speed:real);

{*****************************************}
{****A components used for the vehicle****}
{*****************************************}
{***Procedures pour les �l�ments du v�hicule
en particulier les roues et disques de frein***}

      //A Procedure for the tires
      {Pour dessiner une roue: est compos� de circles}
    procedure tire(x,y:integer);

      //This procedure allow to draw a disc for the brakes
      {Dessine le disque de frein: Usage de tableaux (SD)}
    procedure Disc(coord:TPoint;items:integer;theta:real;MyColor:TColor);

      //The wheelcap. Do you know what is a wheelcap? Lol!
      {Dessine l'enjoliveur ou jante}
    procedure wheelcap(coord:TPoint;theta:real;MyColor:TColor);

      //The Main Car
      {Dessine la carrosserie, incluant les vitres, feux de stop...}
    procedure Main_body(x,y:integer; thetaFw,ThetaBw:real);

    procedure initcanvas;
{*******************************************************}
{****A procedure or functions for the local database****}
{*******************************************************}
       //A procedure for the database
       {Procedure pour la t�l�metrie}
       {Note: Lorsqu'il est activ�, il trace imm�diatement
       la courbe � partir des informations recueillies;
       n'utilise donc PAS de tableaux mais enregistre tout de suite
       sur le graphe: recueille la vitesse du v�hicule et celui d'une roue}
    procedure Telemetry(x,fx:real);

      //Standards
      {Composants utilis�s pour l'interfacing}  
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure GearUpClick(Sender: TObject);
    procedure GearDownClick(Sender: TObject);
    procedure BrakeButtonClick(Sender: TObject);
    procedure WeatherComboBoxChange(Sender: TObject);
    procedure GoTestClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure AutoBrakeCheckBoxClick(Sender: TObject);
    procedure AnalysisButtonClick(Sender: TObject);
    procedure Quitter1Click(Sender: TObject);
    procedure BMW325i1Click(Sender: TObject);
    procedure Porsche9111Click(Sender: TObject);

  Private
    {****************************}
    {****D�clarations priv�es****}
    {****************************}

  //For the principal procedures
    {Utilis� pour le changement de rep�re}
    iScale:Real; //coefficient d'echelle (valeur 1 taille normale)
    ux,uy:integer; //position du v�hicule dans le canevas

  //concerning the vehicle
    v0,brakebias,car_angle:real;
    theta1,theta2:real;
    speed:Real;
    Brake:Boolean;
    distbrake,SlpRate:real;
    CarSelect:String;//Choose the vehicle


  //For the environment
    x_ground,x_bridge:integer;
    clweather:TColor;

  //For the gearbox
    Gearbox:Array[1..5] of Gear;
    iGear:integer;
    incr,incr2,rpm,kph,Maxrpm,t,t2:real;
    UpGear,DownGear:Boolean;
    v,dt:real;

  //Bitmap
      {Weather icon}
    WeatherBmp :TBitMap;
    WeatherIcon:TRect;
    WeatherCdt:String;

      {Pedal icon}
    PedalBmp1, PedalBmp2, PedalBmp3 :TBitmap;
    PedalIcon:TRect;
    pedalposition:integer;
    PedalLoaded:boolean;

  //Telemetry
    Temps:Real;
    Recorder:Boolean;

  Public
    {******************************}
    {****D�clarations publiques****}
    {******************************}
  End;

var
  Form1: TForm1;

implementation

Uses
  GLFile3ds, Jpeg, GLtexture, TEleunit;

{++++++++++++++++++++++++++++++++}
{++++For the gearbox and misc++++}
{++++++++++++++++++++++++++++++++}

{Set the position of the braking pedal}
Procedure TForm1.SetPedalPos(pedalpos:integer);
Begin
  PedalIcon.Left:=0;
  PedalIcon.Right:=Image3.ClientWidth;
  PedalIcon.Top:=0;
  PedalIcon.Bottom:=Image3.ClientHeight;

  If (PedalLoaded=False) then
  begin
    PedalBmp1:=TBitmap.Create;
    PedalBmp2:=TBitmap.Create;
    PedalBmp3:=TBitmap.Create;
    PedalBmp1.loadfromfile('..\ABS3\Texture\Pedal\Pedal1.bmp');
    PedalBmp2.loadfromfile('..\ABS3\Texture\Pedal\Pedal2.bmp');
    PedalBmp3.loadfromfile('..\ABS3\Texture\Pedal\Pedal3.bmp');
    PedalLoaded:=True;
  end;  

  If (pedalpos=1) then
  begin
    try
      Image3.Canvas.StretchDraw(PedalIcon,PedalBmp1);
    finally
    end;
  end;

  If (pedalpos=2) then
  begin
    try
      Image3.Canvas.StretchDraw(PedalIcon,PedalBmp2);
    finally
    end;
  end;

  If (pedalpos=3) then
  begin
    try
      Image3.Canvas.StretchDraw(PedalIcon,PedalBmp3);
    finally
    end;
  end;

End;

{Initializing bitmaps of the weather}
Procedure TForm1.LoadBitmaps;
Begin
  WeatherBmp:=TBitmap.Create;
  With WeatherBmp do
    begin
      loadfromfile('..\ABS3\Texture\Weathers\'+weathercdt+'.bmp');
    end;

  try
  with WeatherBmp do
    begin
        //TransParentColor :=ClWhite;
        WeatherIcon.Left:=0;
        WeatherIcon.Right:=Image2.ClientWidth;
        WeatherIcon.Top:=0;
        WeatherIcon.Bottom:=Image2.ClientHeight;
      //Image2.Canvas.DrawFocusRect(WeatherIcon);
      Image2.Canvas.StretchDraw(WeatherIcon,WeatherBmp);
    end;
  finally
  end;

End;

{A function for the accelleration: implementing the gearbox}
Function TForm1.f(x:real):real;
  Var vmax,a:real;
Begin
  vmax:=Gearbox[iGear].speed;
  a:=Gearbox[iGear].ratio;
  f:=vmax*(1-exp(-x/a));
End;

{A derivation of f(x)}
Function TForm1.fdx(x:real):real;
  Var vmax,a:real;
Begin
  vmax:=Gearbox[iGear].speed;
  a:=Gearbox[iGear].ratio;
  fdx:=vmax*exp(-x/a);
End;

Function TForm1.g(x,v0,a:real):real;
Begin
  If (v0*exp(-a*x)>=5) then g:=v0*exp(-a*x)
  Else g:=-x/4+3;
End;

Procedure TForm1.LoadDisc;
  Var root:string;
Begin
    {Loading Disc components}
  root:='..\ABS3\Disc\';
  DiscSupport.LoadFromFile(root+'Support.3ds');
  DiscEtrier.LoadFromFile(root+'Etrier.3ds');
  DiscPlaquette1.LoadFromFile(root+'Plaquette1.3ds');
  DiscPlaquette2.LoadFromFile(root+'Plaquette2.3ds');
  DiscBrake.LoadFromFile(root+'Disc.3ds');

  With DiscBrake.Material.Texture do
    begin
      Disabled:=False;
      Image.LoadFromFile('..\ABS3\Texture\chromic.jpg');
      MappingMode:=tmmSphere;
    end;

    {Colors}
  With DiscSupport.Material.FrontProperties do
    begin
      With Ambient do
         begin
          Alpha:=1;
          Blue:=0.2;
          Green:=0.2;
          Red:=0.2;
         end;
      With Diffuse do
        begin
          Alpha:=1;
          Blue:=0.8;
          Green:=0.8;
          Red:=0.8;
        end;
      With Emission do
        begin
          Alpha:=1;
          Blue:=0;
          Green:=0;
          Red:=0;
        end;
    end;

  With DiscEtrier.Material.FrontProperties do
    begin
      With Ambient do
         begin
          Alpha:=1;
          Blue:=0.4;
          Green:=0.4;
          Red:=0.4;
         end;
      With Diffuse do
        begin
          Alpha:=1;
          Blue:=0.6;
          Green:=0.6;
          Red:=0.6;
        end;
      With Emission do
        begin
          Alpha:=1;
          Blue:=0.8;
          Green:=0.2;
          Red:=0.2;
        end;
    end;

  With DiscPlaquette1.Material.FrontProperties do
    begin
      With Ambient do
         begin
          Alpha:=1;
          Blue:=0.2;
          Green:=0.2;
          Red:=0.2;
         end;
      With Diffuse do
        begin
          Alpha:=1;
          Blue:=0.164706;
          Green:=0.164706;
          Red:=0.647059;
        end;
      With Emission do
        begin
          Alpha:=1;
          Blue:=0.2;
          Green:=0.2;
          Red:=0.2;
        end;
    end;

  With DiscPlaquette2.Material.FrontProperties do
    begin
      With Ambient do
         begin
          Alpha:=1;
          Blue:=0.2;
          Green:=0.2;
          Red:=0.2;
         end;
      With Diffuse do
        begin
          Alpha:=1;
          Blue:=0.164706;
          Green:=1.64706;
          Red:=0.647059
        end;
      With Emission do
        begin
          Alpha:=1;
          Blue:=0;
          Green:=0;
          Red:=0;
        end;
    end;
End;

//A procedure for the Weather conditions and Parameters for THE ABS
Procedure TForm1.SetWeather(vspeed:integer;var sliprate,cur:real;wcdt:string);
Begin
  {Implementing the ABS}
  IF not(ABSCheckBox.Checked) THEN
    BEGIN
    {Without the ABS, the SlipRate can reaches the 90%}
        If (wcdt='Icy') then
        Begin
            {Icy conditions}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(100*sin(2*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(100*sin(cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(160*sin(0.5*cur));
                      If (SlipRate>100) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;

        If (wcdt='Snowy') then
        Begin
            {Snowy conditions}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(90*sin(2*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(95*sin(cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(130*sin(0.5*cur));
                      If (SlipRate>80) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;

        If (wcdt='Rainy') then
        Begin
            {Rainy conditions}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(70*sin(2*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(80*sin(cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(110*sin(0.5*cur));
                      If (SlipRate>80) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;

        If (wcdt='Sunny') then
        Begin
            {Sunny conditions}
          {Parameters for the Verglace}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(50*sin(2*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(70*sin(cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(130*sin(0.5*cur));
                      If (SlipRate>80) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;
    END
  ELSE
    BEGIN
      {With the ABS, the SlipRate rarely reaches the 80%}
       If (wcdt='Icy') then
        Begin
            {Icy conditions}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(70*sin(4*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(80*sin(2*cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(120*sin(2*cur));
                      If (SlipRate>100) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;

        If (wcdt='Snowy') then
        Begin
            {Snowy conditions}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(60*sin(4*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(70*sin(3*cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(110*sin(3*cur));
                      If (SlipRate>80) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;

        If (wcdt='Rainy') then
        Begin
            {Rainy conditions}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(10*sin(4*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(20*sin(4*cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(60*sin(4*cur));
                      If (SlipRate>80) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
        End;

        If (wcdt='Sunny') then
        Begin
            {Sunny conditions}
          {Parameters for the Verglace}
          Case Vspeed of
            0..9:SlipRate:=0;
            10..34:begin
                     SlipRate:=Abs(5*sin(4*cur));
                     cur:=cur+0.3;
                   end;
            35..50:begin
                     SlipRate:=Abs(20*sin(4*cur));
                     cur:=cur+0.3;
                   end;
            51..220:begin
                      SlipRate:=Abs(50*sin(4*cur));
                      If (SlipRate>80) then SlipRate:=100;
                      cur:=cur+0.3;
                    end;
          end;
    END;
End;
End;

//Rotating the disc
Procedure TForm1.AnimDisc(angspeed:real;braking:boolean);
Begin
  DiscBrake.Turn(angspeed*180/pi);
End;

//To active/deactive the brakedisc: percentage
Procedure TForm1.SetBrakeDisc(Force:Real);
Begin
      If (Force>=10) and (Force<20) then
        begin
          DiscPlaquette1.Position.Y:=0.2;
          DiscPlaquette2.Position.Y:=-0.2;
        end;

      If (Force>=20) and (Force<50) then
        begin
          DiscPlaquette1.Position.Y:=0.3;
          DiscPlaquette2.Position.Y:=-0.3;
        end;

      If (Force>=50) and (Force<70) then
        begin
          DiscPlaquette1.Position.Y:=0.4;
          DiscPlaquette2.Position.Y:=-0.4;
        end;

      If (Force>=70) and (Force<=100) then
        begin
          DiscPlaquette1.Position.Y:=0.5;
          DiscPlaquette2.Position.Y:=-0.5;
        end;

    {Else release the brake}
      If (Force>0) and (Force<10) then
        begin
          DiscPlaquette1.Position.Y:=0;
          DiscPlaquette2.Position.Y:=0;
        end;
End;

//Choosing the coulour of the Weather
Function TForm1.WeatherColor(weather:String):TColor;
Begin
WeatherColor:=ClRed;
  If (weather='Sunny') then WeatherColor:=ClGray;
  If (weather='Rainy') then WeatherColor:=ClNavy;
  If (weather='Snowy') then WeatherColor:=ClGray;
  If (weather='Icy') then WeatherColor:=ClSkyBlue;
End;

{+++++++++++++++++++++++++++++++++++++++++++++++++++++++}
{++++A procedure or functions for the local database++++}
{+++++++++++++++++++++++++++++++++++++++++++++++++++++++}

//A procedure for recording the datas
Procedure TForm1.Telemetry(x,fx:real);
Begin
  {!!!!!!!!}  
End;

{++++++++++++++++++++++++++++}
{++++Principal Procedures++++}
{++++++++++++++++++++++++++++}

//Clearcanvas
Procedure TForm1.Cls;
  Var Wdt,Hgt:integer;
Begin
  Wdt:=Image1.ClientWidth;
  Hgt:=Image1.ClientHeight;

  With Image1.Canvas Do
    begin
        {Colors}
      Pen.Color:=ClWhite;
      Brush.Color:=ClWhite;
        {Drawing}
      Rectangle(0,0,Wdt,Hgt);
    end;
End;

//A Circle for the canvas
Procedure TForm1.Circle(Cen:TPoint;radius:integer);
Begin
  With Image1.Canvas Do
    begin
        {Drawing}
      Ellipse(Cen.X-radius,Cen.Y+radius,Cen.X+radius,Cen.Y-radius);
    end;
End;

//For rescaling
Function TForm1.Scl(Length:real):integer;
Begin
  Scl:=round(iscale*length);
End;

//A double Translation based on an angle,radius and distance
Procedure TForm1.Translate2(var Tx,Ty:integer;r,d:integer;theta:real);
Begin
  Tx:=Round(Tx+(r*cos(theta)-d*sin(theta)));
  Ty:=Round(Ty+(r*sin(theta)+d*cos(theta)));
End;

//An Ucs for the main vehicle
Function TForm1.Ucs(x,y:integer):TPoint;
  Var ix,iy,jx,jy,Tx1,Ty1,Tx2,Ty2,dx,dy,Theta:Real;
      hgt:integer;
Begin
  Theta:=Car_Angle;
  dx:=320*iScale; {!!!}
  dy:=10*iScale;  {!!!}
  hgt:=Image1.ClientHeight;

    //The vectors components
  {The vectors components defined by the iScale and theta}
  ix:=Cos(Theta)*iScale;
  iy:=Sin(Theta)*iScale;
  jx:=-Sin(Theta)*iScale;
  jy:=Cos(Theta)*iScale;

  {A vectors used for Translation:
  Used to replace the base point of rotation from the origin}
  Tx1:=dx*(1-Cos(Theta)); {1}
  Ty1:=-dx*Sin(Theta);

  Tx2:=dy*Sin(Theta);     {2}
  Ty2:=dy*(1-Cos(Theta));

    //The unit vectors of the Ucs
  Ucs.X:=round(x*ix+y*jx+Tx1+Tx2+ux);
  Ucs.Y:=hgt-round(x*iy+y*jy+Ty1+Ty2+uy);
End;

Function TForm1.UcsF(x,y:integer):TPoint;
  Var ix,iy,jx,jy:Real;
      hgt:integer;
Begin
  hgt:=Image1.ClientHeight;
    //The vectors components
  ix:=1*iScale;
  iy:=0*iScale; {Eventually if ...}
  jx:=0*iScale;
  jy:=1*iScale;

    //The unit vectors of the fixed Ucs
  UcsF.X:=round(x*ix+y*jx+ux);
  UcsF.Y:=hgt-round(x*iy+y*jy+uy);
End;

//This last ucs is used for the rest
Function TForm1.UcsG(x,y:integer):TPoint;
  Var ix,iy,jx,jy:Real;
      hgt:integer;
Begin
  hgt:=Image1.ClientHeight;
    //The vectors components
  ix:=1;
  iy:=0; {Eventually if ...}
  jx:=0;
  jy:=1;

    //The unit vectors of the fixed Ucs
  UcsG.X:=round(x*ix+y*jx);
  UcsG.Y:=hgt-round(x*iy+y*jy);
End;

{+++++++++++++++++++++++++++++++++++++++++++++++++}
{++++An elements for the interface environment++++}
{+++++++++++++++++++++++++++++++++++++++++++++++++}

//Speedmeter indicator
Procedure TForm1.Speedmeter(var x_pos:integer;mycolor:TColor);
  Var i,sp,it,Hgt,Wdt,ep:integer;
Begin
  With Image1.Canvas do
    begin
      ep:=10;
      Wdt:=Image1.ClientWidth;
      Hgt:=Image1.ClientHeight;
      it:=2;

        Pen.Width:=1;
        Brush.Style:=BsSolid;
        Brush.Color:=ClGray;
      Rectangle(0,Hgt-ep,Wdt,Hgt-ep-20);

      sp:=-Wdt div it;

      If (x_pos>=Wdt div (it)) Then x_pos:=0;
      If (x_pos<0) Then x_pos:=Wdt div it;

      For i:=1 to (it+1) do
        begin
            Pen.Color:=ClLtGray;
            Brush.Color:=MyColor;
          Rectangle(x_pos+sp,Hgt,x_pos+Wdt div (it*2)+sp,Hgt-ep);
            Brush.Color:=ClCream;
          Rectangle(x_pos+sp+Wdt div (it*2),Hgt,
                    x_pos+Wdt div (it*2)+sp+Wdt div (it*2),Hgt-ep);
          sp:=sp+Wdt div it;
            Pen.Color:=ClBlack;
        end;
    end;
End;

//A mobile background as a Bridge
Procedure TForm1.Bridge(var x_pos:integer);
  Var Hgt,Wdt,sp,i,it,ep:integer;
Begin
  With Image1.Canvas Do
      Begin
        it:=2;
        ep:=30;
        Wdt:=Image1.ClientWidth;
        Hgt:=Image1.ClientHeight;

        //The "garde-corps"
           //Horizontal Lines
          {The Outer}
          Pen.Width:=5;
          Pen.Color:=ClGray;
        Polyline([
          ucsG(-Wdt div it,70),ucsG(4*Wdt div it,70),
          ucsG(4*Wdt div it,70+ep),ucsG(-Wdt div it,70+ep),
          ucsG(-Wdt div it,70+2*ep),ucsG(4*Wdt div it,70+2*ep)
          ]);
          {The Inner}
          Pen.Width:=2;
          Pen.Color:=ClMedGray;
        Polyline([
          ucsG(-Wdt div it,70),ucsG(4*Wdt div it,70),
          ucsG(4*Wdt div it,70+ep),ucsG(-Wdt div it,70+ep),
          ucsG(-Wdt div it,70+2*ep),ucsG(4*Wdt div it,70+2*ep)
          ]);

        sp:=-2*Wdt div It;

        If (x_pos>=Wdt div it) then x_pos:=0;
        If (x_pos<0) then x_pos:=Wdt div it;

          //Vertical lines
        For i:=1 to It+1 Do
          begin
              {Outer}
              Pen.Width:=10;
              Pen.Color:=ClGray;
            MoveTo(x_pos+Wdt div it+sp,Hgt-50);
            LineTo(x_pos+Wdt div it+sp,Hgt-75-2*ep);
              {Inner}
              Pen.Width:=5;
              Pen.Color:=ClMedGray;
            MoveTo(x_pos+Wdt div it+sp,Hgt-50);
            LineTo(x_pos+Wdt div it+sp,Hgt-75-2*ep);

            sp:=sp+Wdt div it;
           end;

           Brush.Color:=ClLtGray;
           Pen.Width:=1;
           Pen.Color:=ClMedGray;
           Rectangle(0,Hgt-30,Wdt,Hgt-50);
      End;
End;

//A procedure for the tachometer
Procedure TForm1.Tachometer(x,y:integer;speed,tmax:real);
  {A local procedure for the needle}
  procedure needle(x,y:integer;theta:real);
  var r1,r2,d1,d2:integer;
  begin
    With Image1.Canvas do
      Begin
          Pen.Color:=ClBlue;
          Brush.Color:=ClSkyBlue;
          r1:=Round(2*Cos(theta));
          r2:=Round(42*Cos(theta));
          d1:=Round(2*Sin(theta));
          d2:=Round(42*Sin(theta));
        Polygon([
          UcsG(x-d1,y+r1),
          UcsG(x+r2,y+d2),
          UcsG(x+d1,y-r1)
         ]);
          Pen.Color:=Clgray;
          Brush.Color:=ClGray;
        Circle(UcsG(x,y),6);
      End;
  end;
  {A local procedure for the indicators}
  procedure indicators(x,y:integer);
  var x1,y1,x2,y2:integer;
       theta:real;
       i:integer;
  begin
    With Image1.Canvas do
      begin
        theta:=-pi/6;
        For i:=1 to 8 do
          begin
            Case i of
              1:Pen.Color:=ClRed;
              2:Pen.Color:=ClYellow;
              3..8:Pen.Color:=ClWhite;
            end;
            x1:=x+round(41*Cos(theta));y1:=y+round(41*Sin(theta));
            x2:=x+round(46*Cos(theta));y2:=y+round(46*Sin(theta));
            Polyline([
              UcsG(x1,y1),UcsG(x2,y2)
            ]);
            theta:=theta+4*pi/21;
          end;
        end;
  end;
  {A Procedure of the tachometer}
Var d:integer;
    theta:real;
Begin
  With Image1.Canvas do
  begin
      //7000 rpm is the max allowed
      theta:=(4*pi/3)/tmax*speed;
    If (speed<0) then theta:=-theta;
    If (theta>4*pi/3) then theta:=4*pi/3;
      Pen.Color:=ClGray;
      Pen.Width:=1;
      Brush.Color:=ClSilver;
    Circle(UcsG(x,y),50);
      Pen.Color:=ClBlack;
      Brush.Color:=ClMoneyGreen;
    Circle(UcsG(x,y),47);
      Pen.Width:=1;
      d:=Image1.ClientHeight;
    Rectangle(x-20,d-y+18,x+20,d-y+28);
      Pen.Width:=2;
    needle(x,y,-theta-5*pi/6);
    indicators(x,y);
  end;
End;

//A procedure for the tachymeter
Procedure TForm1.Tachymeter(x,y:integer;speed:real);
  {A Procedure for the needle}
  procedure needle(x,y:integer;theta:real);
  var r1,r2,d1,d2:integer;
  begin
    With Image1.Canvas do
      Begin
          Pen.Color:=ClRed;
          Brush.Color:=ClSkyBlue;
          r1:=Round(2*Cos(theta));
          r2:=Round(45*Cos(theta));
          d1:=Round(2*Sin(theta));
          d2:=Round(45*Sin(theta));
        Polygon([
          UcsG(x-d1,y+r1),
          UcsG(x+r2,y+d2),
          UcsG(x+d1,y-r1)
         ]);
          Pen.Color:=Clgray;
          Brush.Color:=ClGray;
        Circle(UcsG(x,y),6);
      End;
  end;
  {A Procedure for the indicators}
  procedure indicators(x,y:integer);
  var x1,y1,x2,y2:integer;
       theta:real;
       i:integer;
  begin
    With Image1.Canvas do
      begin
        theta:=-pi/6;
        For i:=1 to 12 do
          begin
            x1:=x+round(43*Cos(theta));y1:=y+round(43*Sin(theta));
            x2:=x+round(49*Cos(theta));y2:=y+round(49*Sin(theta));
            Polyline([
              UcsG(x1,y1),UcsG(x2,y2)
            ]);
            theta:=theta+4*pi/33;
          end;
        end;
  end;
  {A Procedure of the tachymeter}
Var d,tmax:integer;
    theta:real;
Begin
  With Image1.Canvas do
  begin
      tmax:=220;
      //220 kph is the max speed allowed
      theta:=(4*pi/3)/tmax*speed;
    If (speed<0) then theta:=-theta;
    If (theta>4*pi/3) then theta:=4*pi/3;
      Pen.Color:=ClGray;
      Pen.Width:=1;
      Brush.Color:=ClSilver;
    Circle(UcsG(x,y),53);
      Pen.Color:=ClBlack;
      Brush.Color:=ClSkyBlue;
    Circle(UcsG(x,y),50);
      Pen.Width:=1;
      d:=Image1.ClientHeight;
    Rectangle(x-20,d-y+18,x+20,d-y+28);
      Pen.Width:=2;
    needle(x,y,-theta-5*pi/6);
    indicators(x,y);
  end;
End;

{+++++++++++++++++++++++++++++++++++++++++}
{++++A components used for the vehicle++++}
{+++++++++++++++++++++++++++++++++++++++++}

//A procedure for the tires
Procedure TForm1.tire(x,y:integer);
Begin
  With Image1.Canvas do
    begin
        Brush.Color:=ClDkGray;
        Pen.color:=ClBlack;
      Circle(ucsF(x,y),Round(45*iScale));
        Brush.Color:=ClWhite;
      Circle(ucsF(x,y),Round(32*iScale));
        Brush.Color:=ClSilver;
      Circle(ucsF(x,y),Round(30*iScale));
        Brush.Color:=ClDkGray;
      Circle(ucsF(x,y),Round(29*iScale));
    end;
End;

//This procedure allow to draw a disc for the brakes
Procedure TForm1.Disc(coord:TPoint;items:integer;theta:real;MyColor:TColor);
  Var i,x_o,y_o:integer;
      angle:real;
Begin
  With Image1.Canvas do
     begin
       angle:=0;
       Pen.Color:=MyColor;
         Circle(coord,Round(23*iScale));
         Circle(coord,Round(14*iScale));
         Circle(coord,Round(11*iScale));
         Circle(coord,Round(3*iScale));

       For i:=1 to items do
         Begin
             x_o:=coord.X;
             y_o:=coord.Y;
           Translate2(x_o,y_o,scl(14),0,angle+theta);
           MoveTo(x_o,y_o);

             x_o:=coord.X;
             y_o:=coord.Y;
           Translate2(x_o,y_o,scl(17),2,angle+theta);
           LineTo(x_o,y_o);

             x_o:=coord.X;
             y_o:=coord.Y;
           Translate2(x_o,y_o,scl(20),4,angle+theta);
           LineTo(x_o,y_o);

             x_o:=coord.X;
             y_o:=coord.Y;
           Translate2(x_o,y_o,scl(22),7,angle+theta);
           LineTo(x_o,y_o);

           angle:=angle+2*pi/items;
         End;
     end;
End;

//The wheelcap
Procedure TForm1.wheelcap(coord:TPoint;theta:real;MyColor:TColor);
  Var x_w,y_w,i,j,items:integer;
       angle:real;
       pts:points;
Begin
  {Initializing the array of points}
    angle:=0;items:=5;i:=1;
    x_w:=coord.X; y_w:=coord.Y;
    translate2(x_w,y_w,scl(7),scl(-5),0+theta);
      pts[1].X:=x_w;
      pts[1].Y:=y_w;

    For j:=1 to items do
      begin
        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(18),scl(-4),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;


        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(29),scl(-5),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(29),scl(-1),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(7),scl(-1),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(7),scl(1),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(29),scl(1),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(29),scl(5),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        x_w:=coord.X; y_w:=coord.Y;
        translate2(x_w,y_w,scl(18),scl(4),angle+theta);
          i:=i+1;
          pts[i].X:=x_w;
          pts[i].Y:=y_w;

        angle:=angle+2*pi/items;

        If (j<items) Then
          begin
            x_w:=coord.X; y_w:=coord.Y;
            translate2(x_w,y_w,scl(7),scl(-5),angle+theta);
              i:=i+1;
              pts[i].X:=x_w;
              pts[i].Y:=y_w;
          end;
        If (j=items) Then
          begin
            x_w:=coord.X; y_w:=coord.Y;
            translate2(x_w,y_w,scl(7),scl(-5),0+theta);
              pts[i].X:=x_w;
              pts[i].Y:=y_w;
          end;
      end;
  With Image1.Canvas Do
    begin
      Pen.Color:=MyColor;
      {Enjoy!}
      Polygon(pts);
      Circle(coord,5);
    end;
End;

//The Main Car
Procedure TForm1.Main_body(x,y:integer;thetaFw,ThetaBw:real);
Begin
    ux:=x;
    uy:=y;
    With Form1.Image1.Canvas do
      Begin
        {A model of the BMW325I}
        IF (CarSelect='BMW325I') then
        begin
      {****Wheels/Wheelcap//tires****}
      {The Wheels and Tires First because First In Last Seen}
      {Note: The reference is UcsG as UCS}
        {Under}
          Pen.Width:=1;
          Brush.Color:=ClGray;
          Pen.Color:=ClGray;
        Polygon([
          ucs(87,11),ucs(185,0),ucs(277,10),
          ucs(342,10),ucs(454,0),ucs(553,1),
          ucs(553,72),ucs(87,72)
        ]);

        {The tires}
          //Forward tire
        Tire(505,19);
          //Backward tire
        Tire(137,19);

        {The discs}
          Brush.Color:=ClLtGray;
          //Forward disc
        Disc(ucsF(505,19),10,thetaFw,ClBlack);
          //backward disc
        Disc(ucsF(137,19),10,thetaBw,ClBlack);

        {The 'etriers'}
          //Forward 'etrier'
          Brush.Color:=ClBlue;
          Pen.Color:=ClNavy;
        Polygon([
          ucsF(484,3),ucsF(490,3),
          ucsF(490,12),ucsF(489,19),
          ucsF(490,26),ucsF(490,36),
          ucsF(484,36),ucsF(482,33),
          ucsF(482,6)
        ]);
        Polyline([
         ucsF(490,7),ucsF(484,7),
         ucsF(484,31),ucsF(490,31),
         ucsF(490,29),ucsF(485,29),
         ucsF(485,9),ucsF(490,9)
        ]);
          //Backward 'etrier'
        Polygon([
           ucsF(116,3),ucsF(122,3),
           ucsF(122,12),ucsF(121,19),
           ucsF(122,26),ucsF(122,36),
           ucsF(116,36),ucsF(114,33),
           ucsF(114,6)
        ]);
        Polyline([
           ucsF(122,7),ucsF(116,7),
           ucsF(116,31),ucsF(122,31),
           ucsF(122,29),ucsF(117,29),
           ucsF(117,9),ucsF(122,9)
        ]);

        {The wheelcaps}
            Brush.Color:=ClSilver;
          //Forward wheelcap
        Wheelcap(ucsF(505,19),thetaFw,ClGray);
          //Backward wheelcap
        Wheelcap(ucsF(137,19),thetaBw,ClGray);

      {****Main details****}
            Brush.Color:=ClWhite;
            Pen.Color:=ClBlack;
            Pen.Width:=2;
          //The border of the main
        Polygon([
          ucs(87,11),ucs(66,13),ucs(35,18),
          ucs(19,21),ucs(13,26),ucs(5,40),

          ucs(3,46),ucs(0,49),ucs(0,55),
          ucs(12,64),ucs(11,93),ucs(8,113),
        
          ucs(68,121),ucs(86,120),ucs(156,152),
          ucs(150,158),ucs(181,163),ucs(211,165),

          ucs(263,165),ucs(325,159),ucs(342,155),
          ucs(345,153),ucs(338,150),ucs(413,102),

          ucs(438,106),ucs(491,98),ucs(577,80),
          ucs(588,74),ucs(592,70),ucs(597,64),

          ucs(598,47),ucs(600,45),ucs(600,35),
          ucs(598,31),ucs(595,29),ucs(592,28),

          ucs(592,26),ucs(595,24),ucs(591,23),
          ucs(591,20),ucs(594,20),ucs(594,18),

          ucs(591,17),ucs(591,14),ucs(594,14),
          ucs(591,3),ucs(584,1),ucs(553,1),

          ucs(551,26),ucs(546,43),ucs(539,52),
          ucs(529,60),ucs(518,65),ucs(505,67),

          ucs(493,66),ucs(481,62),ucs(472,56),
          ucs(465,50),ucs(456,29),ucs(455,3),

          ucs(454,0),ucs(417,0),ucs(342,6),
          ucs(277,7),ucs(185,0),ucs(184,19),

          ucs(179,40),ucs(173,49),ucs(165,55),
          ucs(155,60),ucs(142,62),ucs(129,61),

          ucs(115,58),ucs(106,54),ucs(97,45),
          ucs(91,33)
        ]);
            Pen.Color:=ClGray;
            Pen.Width:=1;
          //Main(1)
          Polyline([
            ucs(6,39),ucs(91,33),ucs(97,45),
            ucs(1,48),ucs(0,49),ucs(0,55),
            ucs(103,52)
          ]);

          //Main(2)
          Polyline([
            ucs(553,50),ucs(546,60),ucs(534,69),
            ucs(519,74),ucs(505,77),ucs(491,76),
            ucs(476,72),ucs(465,63),ucs(458,53),
            ucs(454,44)
          ]);

          //Main(3)
          Polyline([
            ucs(8,113),ucs(11,112),ucs(15,110),
            ucs(17,108),ucs(20,103),ucs(21,100),
            ucs(22,94)
          ]);

          //Main(4)
          Polyline([
            ucs(20,103),ucs(32,104),ucs(66,105),
            ucs(135,104),ucs(395,96)
          ]);

          //Main(5)
            Pen.color:=ClGray;
          Polyline([
            ucs(148,105),ucs(141,114),ucs(142,120),
            ucs(159,136),ucs(172,145),ucs(183,150),
            ucs(194,154),ucs(211,155),ucs(240,156),
            ucs(263,155),ucs(287,154),ucs(312,151),
            ucs(327,147),ucs(352,133),ucs(392,107),
            ucs(404,99)
          ]);

          //Main(6)
          Polyline([
            ucs(156,152),ucs(168,157),ucs(194,161),
            ucs(240,162),ucs(287,161),ucs(312,158),
            ucs(328,156),ucs(337,153)
          ]);

          //Main(7)
            Pen.color:=clGray;
          Polyline([
            ucs(599,43),ucs(546,43),ucs(549,36),ucs(599,36)
          ]);
          Polyline([
            ucs(551,26),ucs(592,28)
          ]);

          //Main(8)
            Brush.Color:=ClBlack;
            Pen.color:=clBlack;
          Polyline([
            ucs(581,68),ucs(585,69),ucs(589,70),ucs(592,70)
          ]);

          //Main(9)
          Polyline([
            ucs(9,60),ucs(96,57),ucs(101,61),
            ucs(112,67),ucs(128,71),ucs(142,71),
            ucs(157,68),ucs(168,63),ucs(178,53),
            ucs(183,43),ucs(186,30),ucs(186,19),
            ucs(277,19),ucs(455,16)
          ]);

          //Main(10)
            Pen.Color:=ClBlack;
            Brush.Color:=ClBlack;
          Polyline([
            ucs(417,97),ucs(426,97),ucs(440,96),
            ucs(455,95),ucs(476,92),ucs(491,90),
            ucs(506,87),ucs(519,85),ucs(534,82),
            ucs(546,78),ucs(558,75),ucs(563,73),
            ucs(566,71),ucs(568,68)
          ]);

      {****The glasses****}
          //Glass(1) (In the Back as the 'lunette arriere')
            Brush.Color:=ClSkyBlue;
            Pen.color:=clblue;
            Pen.Width:=1;
          Polygon([
            ucs(68,121),ucs(86,120),ucs(156,152),ucs(151,156)
          ]);

          //Glass(5) (In the front as the 'pare-brise')
          Polygon([
            ucs(436,106),ucs(413,102),ucs(338,150),ucs(345,153)
          ]);

      {Glass from the lateral}
          //Glass(4) (Front)
          Polygon([
            ucs(399,98),ucs(349,131),ucs(324,146),
            ucs(312,149),ucs(262,153),ucs(279,100),
            ucs(395,97)
          ]);
          //Glass(3) (Back)
          Polygon([
            ucs(249,153),ucs(211,153),ucs(194,152),
            ucs(181,148),ucs(192,103),ucs(256,100)
          ]);
          //Back(2) (the small)
            Polygon([
              ucs(178,146),ucs(188,103),ucs(149,105),
              ucs(142,114),ucs(143,120),ucs(161,135)
            ]);

      {****Blacktrims****}
          //Mirror Trim
            Pen.Color:=ClBlack;
            Brush.Color:=ClSilver;
          Polygon([
            ucs(380,111),ucs(380,97),ucs(395,97),ucs(399,98)
          ]);
            Pen.Color:=ClBlack;
            Brush.Color:=ClBlack;
          //Blktrim(1)
          Polyline([
            ucs(183,50),ucs(278,47),ucs(320,46),
            ucs(371,45),ucs(423,44),ucs(454,44),
            ucs(454,42),ucs(421,43),ucs(371,43),
            ucs(320,45),ucs(277,46),ucs(184,49),
            ucs(186,46),ucs(278,43),ucs(320,42),
            ucs(371,41),ucs(422,40),ucs(452,39),
            ucs(452,38),ucs(421,39),ucs(371,39),
            ucs(371,40),ucs(320,40),ucs(277,41),
            ucs(186,44)
          ]);
          //Blktrim2
            Pen.Color:=ClGray;
          Polyline([
            ucs(30,92),ucs(66,91),ucs(105,90),
            ucs(145,88),ucs(222,85),ucs(320,82),
            ucs(371,80),ucs(421,78),ucs(476,76),
            ucs(320,81),ucs(222,84),ucs(146,87),
            ucs(105,89),ucs(66,90),ucs(31,91)
          ]);



    {****Doors/details****}
          //The mirror
            {Cache}
            Brush.Color:=ClLtGray;
            Pen.Color:=ClBlack;
          Polygon([
            ucs(370,115),ucs(369,97),ucs(383,97),
            ucs(383,106),ucs(380,113),ucs(378,115)
          ]);
            {Mirror}
          Polyline([
            ucs(378,115),ucs(373,114),ucs(372,105),
            ucs(377,100),ucs(381,99),ucs(383,97)
          ]);
        
            {Backward}
          //Reservoir Cap
            Brush.Color:=ClWhite;
            Pen.Color:=ClSilver;
          Polygon([
            ucs(72,68),ucs(74,66),ucs(93,66),
            ucs(95,68),ucs(95,81),ucs(95,81),
            ucs(93,83),ucs(72,83)
          ]);

          //Door trim(1) (Below)
            Brush.Color:=ClBlack;
            Pen.Color:=ClBlack;
          Polygon([
            ucs(191,19),ucs(190,31),ucs(186,45),
            ucs(179,57),ucs(167,69),ucs(154,76),
            ucs(143,93),ucs(137,105),ucs(136,105),
            ucs(142,94),ucs(153,76),ucs(167,68),
            ucs(178,57),ucs(185,45),ucs(189,30),
            ucs(190,19)
          ]);
          //Door trim(2) (Above)
          Polygon([
            ucs(194,158),ucs(188,154),ucs(182,152),
            ucs(169,146),ucs(155,138),ucs(151,136),
            ucs(138,125),ucs(133,120),ucs(133,117),
            ucs(133,114),ucs(135,109),ucs(136,105),
            ucs(135,105),ucs(135,109),ucs(132,114),
            ucs(132,117),ucs(132,120),ucs(137,125),
            ucs(150,136),ucs(154,138),ucs(169,147),
            ucs(181,152),ucs(187,154),ucs(193,158)
          ]);
            {Middle Vertical Trim as the border}
          //Door trim(3)
          Polygon([
            ucs(257,153),ucs(261,137),ucs(266,115),
            ucs(269,100),ucs(270,98),ucs(274,83),
            ucs(277,69),ucs(278,47),ucs(277,18),
            ucs(276,18),ucs(277,47),ucs(276,69),
            ucs(273,83),ucs(269,98),ucs(268,100),
            ucs(265,115),ucs(260,137),ucs(256,153)
          ]);
            {Front Vertical Trim as the border}
          //Door trim(4)
          Polygon([
            ucs(419,17),ucs(422,39),ucs(423,54),
            ucs(423,65),ucs(422,77),ucs(421,85),
            ucs(419,94),ucs(415,99),ucs(418,94),
            ucs(420,85),ucs(421,77),ucs(422,65),
            ucs(422,54),ucs(421,39),ucs(418,17)
          ]);

          //Handles
            {DoorHandle1(Front))
            Brush.Color:=ClGray;
            Pen.Color:=ClGray;
            {Below part}
          Polygon([
            ucs(286,83),ucs(287,81),ucs(301,80),ucs(303,82)
          ]);
            {Above part}
          Polygon([
            ucs(282,83),ucs(284,87),ucs(286,88),ucs(304,87),
            ucs(306,86),ucs(307,82)
          ]);

            {DoorHandle1(Back))
            {Below part}
          Polygon([
            ucs(165,87),ucs(166,85),ucs(180,84),ucs(182,86)
          ]);
            {Above part}
          Polygon([
            ucs(161,87),ucs(186,86),ucs(185,90),ucs(183,91),
            ucs(165,92),ucs(163,91)
          ]);

            {Griltrim}
          //Griltrim
            Pen.color:=clGray;
          Polygon([
            ucs(591,68),ucs(591,64),ucs(592,55),
            ucs(593,48),ucs(597,48),ucs(596,64),
            ucs(594,68)
          ]);

      {****Lights****}
          //Hlight
              Brush.Color:=ClSkyBlue;
              Pen.Color:=clMedgray;
          Polygon([
            ucs(568,67),ucs(570,65),ucs(571,59),
            ucs(572,55),ucs(572,50),ucs(585,50),
            ucs(585,55),ucs(584,59),ucs(583,64),
            ucs(581,67)
          ]);
          //Tlight
            Brush.Color:=ClMoneyGreen;
            Pen.Color:=clMoneyGreen;
          Polygon([
            ucs(565,67),ucs(562,66),ucs(560,64),
            ucs(559,63),ucs(557,59),ucs(556,55),
            ucs(555,50),ucs(572,50),ucs(572,55),
            ucs(571,59),ucs(570,65),ucs(568,67)
          ]);
            {Lateral}
          Polygon([
              ucs(440,50),ucs(452,50),ucs(452,46),ucs(440,46)
          ]);

          //Taillight
            Brush.Color:=ClRed;
            Pen.Color:=ClMaroon;
          Polygon([
            ucs(33,66),ucs(36,68),ucs(37,70),
            ucs(35,79),ucs(31,91),ucs(28,94),
            ucs(20,95),ucs(17,94),ucs(15,91),
            ucs(14,81),ucs(14,71),ucs(14,66)
          ]);
        end
        {A model of the Porsche 911}
        ELSE IF (CarSelect='PORSCHE911') THEN
          begin
      {****Wheels/Wheelcap//tires****}
      {The Wheels and Tires First because First In Last Seen}
      {Note: The reference is UcsG as UCS}
        {Under}
        Pen.Width:=1;
        Brush.Color:=ClGray;
        Pen.Color:=ClGray;
      Polygon([
        ucs(94,6),ucs(94,74),ucs(507,74),
        ucs(507,3),ucs(401,-1),ucs(202,-1)
      ]);

      {The tires}
        //Forward tire
      Tire(457,21);
        //Backward tire
      Tire(148,21);

      {The discs}
       Brush.Color:=ClLtGray;
        //Forward disc
      Disc(ucsF(457,19),10,thetaFw,ClBlack);
        //backward disc
      Disc(ucsF(148,19),10,thetaBw,ClBlack);

      {The etriers}

      {The wheelcaps}
        Brush.Color:=ClSilver;
        //Forward wheelcap
      Wheelcap(ucsF(457,21),thetaFw,ClGray);
        //Backward wheelcap
      Wheelcap(ucsF(148,21),thetaBw,ClGray);

      {****Main details****}
          Brush.Color:=ClWhite;
          Pen.Color:=ClBlack;
          Pen.Width:=2;
        //The border of the main
      Polygon([
        ucs(94,6),ucs(72,7),ucs(51,9),
        ucs(33,13),ucs(21,17),ucs(19,22),

        ucs(7,23),ucs(2,23),ucs(0,28),
        ucs(0,35),ucs(2,40),ucs(11,43),

        ucs(12,58),ucs(24,70),ucs(63,94),
        ucs(87,107),ucs(99,112),ucs(101,111),

        ucs(101,109),ucs(103,109),ucs(185,137),
        ucs(186,139),ucs(186,143),ucs(203,148),

        ucs(234,152),ucs(277,155),ucs(311,153),
        ucs(317,149),ucs(314,147),ucs(313,144),

        ucs(314,140),ucs(352,98),ucs(355,96),
        ucs(362,96),ucs(376,97),ucs(384,99),

        ucs(387,100),ucs(415,96),ucs(443,91),
        ucs(474,89),ucs(508,85),ucs(530,82),

        ucs(532,81),ucs(539,79),ucs(544,56),
        ucs(570,42),ucs(577,41),ucs(582,38),

        ucs(582,26),ucs(581,23),ucs(575,21),
        ucs(571,13),ucs(567,8),ucs(537,8),

        ucs(517,7),ucs(510,7),ucs(507,3),
        ucs(502,41),ucs(495,51),ucs(487,59),

        ucs(478,65),ucs(467,68),ucs(457,70),
        ucs(445,68),ucs(445,68),ucs(434,65),

        ucs(425,58),ucs(417,50),ucs(411,40),
        ucs(404,16),ucs(404,10),ucs(403,9),

        ucs(402,0),ucs(401,-2),ucs(202,-2),
        ucs(198,9),ucs(198,10),ucs(197,16),

        ucs(194,33),ucs(189,45),ucs(182,55),
        ucs(170,63),ucs(157,67),ucs(142,67),

        ucs(128,64),ucs(115,58),ucs(107,49),
        ucs(103,42),ucs(96,15),ucs(94,6)
      ]);
        Pen.Color:=ClGray;
        Pen.Width:=1;
      //Main(1)
      Polyline([
        ucs(96,15),ucs(53,19),ucs(33,22),
        ucs(33,29),ucs(0,29),ucs(0,35),
        ucs(33,35),ucs(33,40),ucs(30,42),
        ucs(14,42),ucs(11,43)
      ]);

      //Main(2)
      Polyline([
        ucs(103,42),ucs(52,42),ucs(46,58),
        ucs(71,68),ucs(123,81),ucs(172,84),
        ucs(182,86),ucs(185,94)
      ]);

      //Main(3) as the griltrim
        Brush.Color:=ClGray;
      Polygon([
        ucs(63,94),ucs(65,93),ucs(84,101),
        ucs(86,103),ucs(87,105),ucs(87,107)
      ]);
        Brush.Color:=ClWhite;
      //Main(4)
      Polygon([
        ucs(347,94),ucs(350,97),ucs(310,141),
        ucs(303,143),ucs(277,143),ucs(262,142),
        ucs(234,139),ucs(218,136),ucs(190,130),
        ucs(169,122),ucs(157,116),ucs(149,111),
        ucs(145,107),ucs(144,105),ucs(144,102),
        ucs(146,99),ucs(151,97),ucs(157,96)
      ]);

      //Main(5)
      Polyline([
        ucs(223,94),ucs(221,88),ucs(223,80),
        ucs(230,53),ucs(239,33),ucs(249,16),
        ucs(197,16),ucs(404,16),ucs(384,16),
        ucs(385,33),ucs(386,53),ucs(385,80),
        ucs(378,88),ucs(378,94),ucs(355,96),
        ucs(378,94),ucs(401,93),ucs(428,92),
        ucs(443,91)
      ]);

      //Main(6)
      Polyline([
        ucs(404,16),ucs(404,33),ucs(405,43),
        ucs(410,54),ucs(418,66),ucs(430,74),
        ucs(444,78),ucs(457,79),ucs(470,78),
        ucs(482,73),ucs(492,66),ucs(501,54),
        ucs(507,41),ucs(510,7)
      ]);

      //Main(7)
      Polyline([
        ucs(568,42),ucs(548,42),ucs(535,41),
        ucs(534,38),ucs(536,26),ucs(537,20),
        ucs(552,20),ucs(575,21)
      ]);

      //Main(8)
      Polyline([
        ucs(582,38),ucs(534,38),ucs(536,22),
        ucs(561,22),ucs(581,23),ucs(582,26),
        ucs(538,25),ucs(537,36),ucs(582,38)
      ]);

      //Main(9)
      Polyline([
        ucs(198,10),ucs(404,10),ucs(403,8),ucs(198,8)
      ]);

      //Main(10) 'near the hlights'
      Polyline([
        ucs(532,81),ucs(542,55),ucs(544,56)
      ]);

      //Main(11)
      Polyline([
        ucs(507,41),ucs(512,41),ucs(536,43),
        ucs(550,44),ucs(566,44)
      ]);

      {****The glasses****}
      //Glass(1) (In the Back as the 'lunette arriere')
        Brush.Color:=ClSkyBlue;
        Pen.Color:=Clblue;
        Pen.Width:=1;
      Polygon([
        ucs(103,109),ucs(101,111),ucs(119,119),
        ucs(164,134),ucs(186,140),ucs(185,137)
      ]);

      //Glass(5) (In the front as the 'pare-brise')
      Polygon([
        ucs(384,99),ucs(370,110),ucs(327,143),
        ucs(317,149),ucs(314,147),ucs(313,144),
        ucs(314,140),ucs(352,98),ucs(355,96),
        ucs(362,96),ucs(376,97)
      ]);

      {Glass from the lateral}
      //Glass (Front)
        Polygon([
          ucs(345,97),ucs(346,97),ucs(309,138),
          ucs(307,140),ucs(303,141),ucs(292,141),
          ucs(262,140),ucs(250,139),ucs(220,134),
          ucs(227,97)
        ]);
      //Glass (Back)
        Polygon([
          ucs(215,134),ucs(203,131),ucs(189,127),
          ucs(177,122),ucs(157,113),ucs(149,108),
          ucs(146,105),ucs(146,102),ucs(148,100),
          ucs(152,99),ucs(157,98),ucs(186,97),
          ucs(222,97)
        ]);

      {****Doors/details****}
        //the mirror
          Brush.Color:=ClLtGray;
          Pen.Color:=ClBlack;
        Polygon([
          ucs(351,82),ucs(356,82),ucs(357,91),
          ucs(357,95),ucs(355,109),ucs(344,112),
          ucs(342,110),ucs(342,96),ucs(344,94),
          ucs(350,95),ucs(347,91)
        ]);
        Polyline([
          ucs(353,82),ucs(355,97),ucs(346,96),
          ucs(344,98),ucs(344,108),ucs(345,110),
          ucs(355,109)
        ]);

        //Door handle
          Brush.Color:=ClGray;
          Pen.Color:=ClGray;
        Polygon([
          ucs(234,76),ucs(235,74),ucs(238,73),
          ucs(244,73),ucs(246,74),ucs(248,75),
          ucs(248,78),ucs(246,80),ucs(244,81),
          ucs(238,81),ucs(235,80),ucs(234,79)
        ]);
          Brush.Color:=ClLtGray;
        Polygon([
          ucs(229,79),ucs(232,79),ucs(235,78),
          ucs(244,78),ucs(246,79),ucs(248,79),
          ucs(249,78),ucs(249,76),ucs(248,75),
          ucs(246,75),ucs(244,76),ucs(235,76),
          ucs(232,75),ucs(229,75),ucs(228,76),
          ucs(228,78)
        ]);

        {****Lights****}
        //Tlight
          Brush.Color:=ClMoneyGreen;
          Pen.Color:=ClMoneyGreen;
        Polygon([
          ucs(537,35),ucs(560,35),
          ucs(560,27),ucs(538,27)
        ]);

        //Taillight
          Brush.Color:=ClRed;
          Pen.Color:=ClMaroon;
        Polygon([
          ucs(12,56),ucs(30,56),ucs(30,44),ucs(12,44)
        ]);
        Polygon([
          ucs(31,56),ucs(44,56),ucs(49,44),ucs(31,44)
        ]);
          end;
      End;
End;

Procedure TForm1.initcanvas;
Begin
  Cls;
  //Showing the environment
  bridge(x_bridge);
  speedmeter(x_ground, ClWeather);
  
  //Showing the car
  Car_Angle:=0;
  iScale:=1;
  Theta1:=0;
  theta2:=0.5;
  Main_body(10,34,theta1,theta2);

  //Initializing the tachymeter and the tachometer
  Tachometer(450,350,0,GearBox[1].speed);
  Tachymeter(570,350,kph);
End;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  CarSelect:='BMW325I';
  temps:=0;
  Recorder:=False;

  AnalysisButton.Enabled:=False;
  DiscStatus.Caption:='Pas de blocage';

  GearUp.Enabled:=False;
  GearDown.Enabled:=False;
  BrakeButton.Enabled:=False;
  AutoBrakeCheckBox.Checked:=False;
  InputKmhEdit.Enabled:=False;

    {Initializing Weather conditions in the WeatherComboBox}
  With WeatherComboBox.Items do
    begin
      Add('Ensoleill�e');
      Add('Pluvieuse');
      Add('Enneig�e');
      Add('Verglac�e');
    end;
    {There are 4 Weather conditions: Icy, Snowy, Rainy, Sunny}
  WeatherCdt:='Sunny';
  ClWeather:=WeatherColor(WeatherCdt);

  DistBrake:=0;
  PedalLoaded:=False;
  LoadBitmaps;
  SetPedalPos(1);
  t2:=0;
  LoadDisc;
  SlpRate:=0;
  Brake:=False;
  BrakeBias:=1050/StrToFloat(CarWeightEdit.Text);

  //For the background
  x_bridge:=0;
  x_ground:=0;
  bridge(x_bridge);
  speedmeter(x_ground, ClWeather);

  //Showing the car
  Car_Angle:=0;
  iScale:=1;
  Theta1:=0;
  theta2:=0.5;
  Main_body(10,34,theta1,theta2);

  //Interface

  //For the Gearbox
    {GearSpeed characteristics}
  Gearbox[1].speed:=60;Gearbox[1].ratio:=1;
  Gearbox[2].speed:=100;Gearbox[2].ratio:=3;
  Gearbox[3].speed:=140;Gearbox[3].ratio:=6;
  Gearbox[4].speed:=190;Gearbox[4].ratio:=9;
  Gearbox[5].speed:=220;Gearbox[5].ratio:=12;

    {incrementation for accelleration}
  incr:=0.01;
    {incrementation for braking}
  incr2:=0.01;
  Rpm:=60;
  kph:=0;

  Tachometer(450,350,0,GearBox[1].speed);
  Tachymeter(570,350,kph);

  UpGear:=False;
  DownGear:=False;
  iGear:=1;

end;

procedure TForm1.Timer1Timer(Sender: TObject);
  Var distUnit:Real;
begin
Cls;
{***************************}
{****Accelleration phase****}
{***************************}

BrakeBias:=1050/StrToFloat(CarWeightEdit.Text);{!!!}

  If (Brake=False) Then
    Begin
      {implementation of the Gearbox}
        {Up Gear}
      If (UpGear=True) Then
        begin
            If (incr<0) Then incr:=-incr;
          MaxRpm:=GearBox[iGear].speed;
          If (Rpm<MaxRpm) Then Rpm:=Rpm+MaxRpm/50
          Else UpGear:=False;
        end;

        {Down Gear}
      If (DownGear=True) Then
        begin
          //
        end;

      {Accelleration incrementation}
      If (t<=40) then t:=t+incr;
      {The incrementation stops when the speed reaches zero}
      If (f(t)<=0) Then incr:=0;

      //For displaying
        {For the vehicle movement}
      speed:=f(t);

      bridge(x_bridge);
      speedmeter(x_ground, clweather);
      x_bridge:=round(x_bridge-speed);
      x_ground:=round(x_ground-speed);

      Main_body(10,34,theta1,theta2);
      theta1:=theta1+speed/45;
      theta2:=theta2+speed/45;
      Animdisc(speed/45,True);

        {Displaying the tachymeters}
      Tachometer(450,350,speed,Rpm);
      Tachymeter(570,350,speed);

      If (Car_angle<fdx(t)/700)
        then
          begin
            If (car_angle<=0.02) and (f(t)<100)
             then Car_angle:=Car_angle+0.0015;
          end
        Else
          Car_angle:=Car_angle-0.001;

      {Memorizing last speed}
      v0:=f(t);

      {SHOWING Speed during accelleration}
      With Image1.Canvas do
        begin
          Brush.Color:=ClWhite;
          Font.Color:=ClBlue;
          TextOut(0,0,Format('v: %2.2f',[speed])+' kmh');
        end;

      {When AutoBrake is enabled}
      If (AutoBrakeCheckBox.Checked=True) then
        begin
          If (StrToFloat(InputKmhEdit.text)<=50) then
            InputKmhEdit.text:='50';
          If (StrToFloat(InputKmhEdit.text)>=210) then
            InputKmhEdit.text:='210';
          If (f(t)>=StrToFloat(InputKmhEdit.text)) then
            BrakeButton.Click;
        end;

      {When the Gear is Automatic}
      If (GearAutoCheckBox.Checked) Then
        begin
          If (f(t)>=GearBox[iGear].speed-10) then
            GearUp.Click;
        end;  

    End
  Else
    Begin
        {Showing the speed before braking}
      With Image1.Canvas do
        begin
          Brush.Color:=ClWhite;
          Font.Color:=ClRed;
          Font.Style:=[];
          TextOut(0,15,Format('vitesse avant freinage: %2.2f',[v0])+' kmh');
        end;
{*********************}
{****Braking phase****}
{*********************}
      //Brakebias - Speed
      //  0.5     -  100
      distunit:=1;
      distbrake:=distbrake+distunit;

      bridge(x_bridge);
      speedmeter(x_ground, clweather);
      Main_body(10,34,theta1,theta2);
      x_bridge:=round(x_bridge-speed);
      x_ground:=round(x_ground-speed);

      SlipRateBar.Position:=Round(SlpRate);

      {STOPS when the speed is not sufficient}
      {Condition d'arret}
      If (g(t,v0,brakebias)>20) then speed:=round(g(t,v0,0.5))
      Else If (WeatherCdt<>'Icy') and (WeatherCdt<>'Snowy')
        then speed:=speed-0.5
        Else speed:=speed-0.2;
      {If STOPPING then}
      If (speed<0) then
        begin
          speed:=0;
          Timer1.Enabled:=False;
          With Image1.Canvas do
            begin
              Brush.Color:=ClWhite;
              Font.Color:=ClRed;
              Font.Style:=[fsBold];
              TextOut(0,30,Format('Distance de freinage: %2.2f',[distbrake])+' unit�s');
              InputKmhEdit.Enabled:=True;
              CarWeightEdit.Enabled:=True;
              AnalysisButton.Enabled:=True;
            end;
        end;

      theta1:=theta1+speed*(100-SlpRate)/(100*45);
      theta2:=theta2+speed*(100-SlpRate)/(100*45);
      Animdisc(speed*(100-SlpRate)/(100*45),True);

        {Displaying the tachymeters}
      Tachometer(450,350,speed,Rpm);
      Tachymeter(570,350,speed);

        {Sliprate depending the weather condition: With or without ABS}
      SetWeather(round(speed),slprate,t2,WeatherCdt);
      SetBrakeDisc(SlpRate);
        {Verifying if the disc is blocked}
          If (SlpRate=100) Then DiscStatus.Caption:='Roue bloqu�e'
          Else DiscStatus.Caption:='Pas de blocage';
        {Implementing the ABS}
            If (SlpRate>90) then
              begin
                Pedalposition:=3;
                incr2:=0.005;
              end;
            If (SlpRate>48) and (SlpRate<52) then
              begin
                Pedalposition:=2;
                incr2:=0.02;
              end;
            If (SlpRate<5) then
              begin
                Pedalposition:=1;
                incr2:=0.04;
              end;
        {ABS here}
      {'Pas besoin de pomper quand l'ABS est activ�;
      appuyer continuellement sur le p�dal de frein}
      IF (ABSCheckBox.Checked=True) THEN Pedalposition:=3;
          
      SetPedalPos(Pedalposition);

      t:=t+incr2;

    {SHOWING the speed during braking}
    With Image1.Canvas do
      begin
        Brush.Color:=ClWhite;
        Font.Color:=ClBlue;
        Font.Style:=[];
        TextOut(0,0,Format('speed: %2.2f',[speed])+' kph');
      end;

    {Approaching the disc etrier when braking}
    If Target1.Position.X<10 then
      begin
        Target1.Position.X:=Target1.Position.X+1;
        GLCamera1.Position.Z:=GLCamera1.Position.Z-0.1;
        GLCamera1.AdjustDistanceToTarget(0.95);
      end
    Else DiscEtrier.Visible:=False;

    {A Recorder for the telemetry}
    If (Recorder) then
      begin
        temps:=temps+1;
        TEleunit.Tmetrie.Polyline1.Nodes.AddNode(temps/85,speed/70,0);
        TEleunit.Tmetrie.Polyline2.Nodes.AddNode(temps/85,speed*(100-SlpRate)/3000,0);
        If (temps=380) then Recorder:=False;
      end;

    End;
End;


{GearUp Event}
procedure TForm1.GearUpClick(Sender: TObject);
begin
  If (iGear<5) Then
    begin
      dt:=0;
      v:=f(t);
      iGear:=iGear+1;
      UpGear:=True;
      GearLabel.Caption:='Vitesse: '+IntToStr(iGear);
      While (f(dt)<v) do
        dt:=dt+incr;
      t:=dt;
    end;
end;

procedure TForm1.GearDownClick(Sender: TObject);
begin
  If (iGear>1) Then
    begin
        dt:=50;
      v:=GearBox[iGear-1].speed;
      DownGear:=True;
      GearLabel.Caption:='Gear: '+IntToStr(iGear-1);
    end;  
end;

procedure TForm1.BrakeButtonClick(Sender: TObject);
begin
  If (Brake=False) then t:=0;
  Brake:=True;
  GearUp.Enabled:=False;
  Recorder:=True;
end;

procedure TForm1.WeatherComboBoxChange(Sender: TObject);
begin
  {M�t�o ensoleill�e}
  If (WeatherComboBox.text='Ensoleill�') then
    begin
      WeatherCdt:='Sunny';
      ClWeather:=WeatherColor(WeatherCdt);
      WeatherTypeLabel.Caption:='Bonne adh�rence';
    end;
  {M�t�o pluvieuse}
  If (WeatherComboBox.text='Pluvieuse') then
    begin
      WeatherCdt:='Rainy';
      ClWeather:=WeatherColor(WeatherCdt);
      WeatherTypeLabel.Caption:='Adh�rence moyenne';
    end;
  {M�t�o enneig�e}
  If (WeatherComboBox.text='Enneig�e') then
    begin
      WeatherCdt:='Snowy';
      ClWeather:=WeatherColor(WeatherCdt);
      WeatherTypeLabel.Caption:='Glissante';
    end;
  {M�t�o Verglac�e}
  If (WeatherComboBox.text='Verglac�e') then
    begin
      WeatherCdt:='Icy';
      ClWeather:=WeatherColor(WeatherCdt);
      WeatherTypeLabel.Caption:='Tr�s gilssante';
    end;
  LoadBitmaps;

  speedmeter(x_ground, clweather);
end;

procedure TForm1.GoTestClick(Sender: TObject);
begin
  With Timer1 do
    begin
      If (Enabled=False) then Enabled:=True;
    end;
  //Preparing for the test
  GoTest.Enabled:=False;
  InputkmhEdit.Enabled:=False;
  CarWeightEdit.Enabled:=False;
  GearUp.Enabled:=True;
  GearDown.Enabled:=False;
  BrakeButton.Enabled:=True;

  //Input parameters
    {Car Weight}
  If (StrToFloat(CarWeightEdit.text)<=500)
    then CarWeightEdit.Text:='500';
  If (StrToFloat(CarWeightEdit.text)>=5000)
    then CarWeightEdit.Text:='5000';
  BrakeBias:=1050/StrToFloat(CarWeightEdit.Text);
  
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Cls;
  temps:=0;
  Recorder:=False;
  {Deleting Nodes}
  TEleunit.Tmetrie.Polyline1.Nodes.Clear;
  TEleunit.Tmetrie.Polyline2.Nodes.Clear;

  DiscStatus.Caption:='No blocage';
  AnalysisButton.Enabled:=False;

  InputKmhEdit.Enabled:=True;
  CarWeightEdit.Enabled:=True;
  GearLabel.Caption:='Vitesse: 1';
  DistBrake:=0;
  GoTest.Enabled:=True;
  Timer1.Enabled:=False;
  {The timer reseter}
  Timer2.Enabled:=True;

  {Reinitializing}
  GearUp.Enabled:=False;
  GearDown.Enabled:=False;
  BrakeButton.Enabled:=False;
  SlipRateBar.Position:=0;

  SetPedalPos(1);
  t:=0;
  t2:=0;
  LoadDisc;
  SlpRate:=0;
  Brake:=False;

  //For the background
  x_bridge:=0;
  x_ground:=0;
  bridge(x_bridge);
  speedmeter(x_ground, clweather);

  //Showing the car
  Car_Angle:=0;
  Theta1:=0;
  theta2:=0.5;
  Main_body(10,34,theta1,theta2);

    {incrementation for accelleration}
  incr:=0.01;
    {incrementation for braking}
  incr2:=0.01;
  Rpm:=60;
  kph:=0;

  Tachometer(450,350,0,GearBox[1].speed);
  Tachymeter(570,350,kph);

  UpGear:=False;
  DownGear:=False;
  iGear:=1;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
  
  {Reseting the view of the discetrier when braking}
    If (Target1.Position.X>0) then
      begin
        Target1.Position.X:=Target1.Position.X-1;
        GLCamera1.Position.Z:=GLCamera1.Position.Z+0.1;
        GLCamera1.AdjustDistanceToTarget(1.05);
      end
    Else
      begin
        DiscEtrier.Visible:=True;
        Timer2.Enabled:=False;
        {Stop Itself when timeout}
      end;
end;

procedure TForm1.AutoBrakeCheckBoxClick(Sender: TObject);
begin
  If (AutoBrakeCheckBox.Checked=True) Then InputKmhEdit.Enabled:=True
  Else InputKmhEdit.Enabled:=False;
end;

procedure TForm1.AnalysisButtonClick(Sender: TObject);
begin
  TEleunit.Tmetrie.Show;
end;

procedure TForm1.Quitter1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.BMW325i1Click(Sender: TObject);
begin
  //Gearbox Characteristics of the BMW 325i
    {GearSpeed characteristics}
  Gearbox[1].speed:=60;Gearbox[1].ratio:=1;
  Gearbox[2].speed:=92;Gearbox[2].ratio:=3;
  Gearbox[3].speed:=130;Gearbox[3].ratio:=6;
  Gearbox[4].speed:=160;Gearbox[4].ratio:=9;
  Gearbox[5].speed:=200;Gearbox[5].ratio:=12;
  CarWeightEdit.Text:='2100';

  CarSelect:='BMW325I';
  initcanvas;
end;

procedure TForm1.Porsche9111Click(Sender: TObject);
begin
  //Gearbox Characteristics of the Porsche 911 (1967)
    {GearSpeed characteristics}
  Gearbox[1].speed:=60;Gearbox[1].ratio:=1.;
  Gearbox[2].speed:=92;Gearbox[2].ratio:=3.5;
  Gearbox[3].speed:=130;Gearbox[3].ratio:=6;
  Gearbox[4].speed:=160;Gearbox[4].ratio:=9;
  Gearbox[5].speed:=200;Gearbox[5].ratio:=11;
  CarWeightEdit.Text:='1075';

  //Select the car
  CarSelect:='PORSCHE911';
  initcanvas;
end;

end.
