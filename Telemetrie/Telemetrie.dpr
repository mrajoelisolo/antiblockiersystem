program Telemetrie;

uses
  Forms,
  TEleunit in 'TEleunit.pas' {Tmetrie},
  Abs_3D in '..\ABS_Bmw ox1.52\Abs_3D.pas' {FormABSPro},
  MainABS in '..\ABS\MainABS.pas' {MainForm},
  about in '..\ABS\About\about.pas' {AboutBox};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TTmetrie, Tmetrie);
  Application.CreateForm(TFormABSPro, FormABSPro);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.
